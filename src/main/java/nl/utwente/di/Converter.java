package nl.utwente.di;

public class Converter {
    public double getDegreesInF(String degrees) {
        return Double.parseDouble(degrees)*1.8 + 32;
    }
}
